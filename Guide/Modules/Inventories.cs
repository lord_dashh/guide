using System;
using System.Linq;
using System.Threading.Tasks;

using DSharpPlus;
using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;
using DSharpPlus.Entities;
using DSharpPlus.Interactivity;

using Guide.Helpers;
using Guide.Entities.SQL;

using SQLite;

namespace Guide.Modules {
    [Group("inventory"), Aliases("in")]
    public class Inventories : BaseCommandModule {
        private SQLiteAsyncConnection DbConnection { get; }

        public Inventories(SQLiteAsyncConnection conn) {
            this.DbConnection = conn;
        }

        [GroupCommand, Description("see the list of items you have")]
        public async Task ExecuteAsync(CommandContext ctx) {
            ItemBlob inventory = await ctx.Member.GetInventoryFromCacheAsync(this.DbConnection);

            DiscordEmbedBuilder embed = new DiscordEmbedBuilder()
                .WithTitle("Items")
                .WithDescription(string.Join("\n", inventory.Items.Select(kv => {
                    DiscordEmoji emoji;
                    if (kv.Value.Type == ItemType.Melee)
                        emoji = DiscordEmoji.FromName(ctx.Client, ":crossed_swords:");
                    else if (kv.Value.Type == ItemType.Ranged)
                        emoji = DiscordEmoji.FromName(ctx.Client, ":bow_and_arrow:");
                    else emoji = DiscordEmoji.FromName(ctx.Client, ":star2:");

                    return $"`{kv.Value.Id}:{kv.Value.Count}`: {emoji} **{kv.Value.Name}**    **`{kv.Value.Damage}`**";
                })));


            await ctx.RespondAsync(embed: embed);
        }
    }
}
