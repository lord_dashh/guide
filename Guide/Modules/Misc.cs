using System;
using System.Diagnostics;
using System.Threading.Tasks;

using DSharpPlus;
using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;
using DSharpPlus.Entities;

namespace Guide.Modules {
    public class Owner : BaseCommandModule {
        [Command("ping"), Description("Checks ping."), Cooldown(3, 1, CooldownBucketType.User)]
        public async Task PingAsync(CommandContext ctx) {
            Stopwatch sw = Stopwatch.StartNew();
            DiscordMessage msg = await ctx.RespondAsync("Pinging");
            sw.Stop();

            await msg.ModifyAsync($"{sw.ElapsedMilliseconds} ms.");
        }
    }
}
