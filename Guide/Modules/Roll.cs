using System;
using System.Threading.Tasks;

using DSharpPlus;
using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;
using DSharpPlus.Entities;

using Guide.Entities.SQL;
using Guide.Helpers;

using SQLite;

namespace Guide.Modules {
    public class Roll : BaseCommandModule {
        private SQLiteAsyncConnection DbConnection { get; set; }

        public Roll(SQLiteAsyncConnection conn) {
            this.DbConnection = conn;
        }

        public override async Task BeforeExecutionAsync(CommandContext ctx) {
            if(ctx.Command.Name != "ask") {
                long guildId = (long) ctx.Guild.Id;
                Server server = await DbConnection.Table<Server>().FirstOrDefaultAsync(s => s.Id == guildId);
                if(server == null || !server.StatsEnabled) {
                    await ctx.RespondAsync("Stats are not enabled on this guild. Ask your local server admin to enable it!");
                    throw new InvalidOperationException("stats disabled for guild");
                }
            }
        }

        [Command("ask"), Aliases("8b"), Description("yes or no answer - useful for rp actions")]
        public async Task YesOrNoAsync(CommandContext ctx, [RemainingText] string whatever) {
            await ctx.RespondAsync(new Random().Next(2) == 0 ? "yes" : "no");
        }

        [Command("roll12"), Aliases("roll"), Description("roll a 12 sided dice")]
        public async Task Roll12Async(CommandContext ctx, [RemainingText] string reason = "") {
            int rollCur = DiceBag.Roll(Dice.D12);
            int rollOther = DiceBag.Roll(Dice.D12);
            string result;

            if (rollCur > rollOther)
                result = "win";
            else if (rollCur == rollOther)
                result = "tie";
            else result = "lose";


            DiscordEmbedBuilder BuildIntermissionEmbed(string roller) {
                return new DiscordEmbedBuilder()
                    .WithDescription($"**`{roller}`** `rolls a dice`")
                    .WithColor(DiscordColor.NotQuiteBlack);
            };

            DiscordEmbedBuilder intermissionEmbed = BuildIntermissionEmbed(ctx.Member.DisplayName);

            Random rng = new Random();

            DiscordMessage msg = await ctx.RespondAsync(embed: intermissionEmbed);
            await Task.Delay(1500 + rng.Next(500));

            DiscordEmbedBuilder rollEmbed = intermissionEmbed
                .WithDescription(intermissionEmbed.Description + $": **{rollCur}**\n"
                    + (reason != "" ? $"- *{reason}*" : ""));

            await msg.ModifyAsync(null, rollEmbed.Build());

            await Task.Delay(500 + rng.Next(150));
            // roll target
            rollEmbed = rollEmbed.AddField("target", "**`target`** `rolls a dice`");

            await msg.ModifyAsync(null, rollEmbed.Build());
            await Task.Delay(1500 + rng.Next(500));

            DiscordEmbedBuilder tgtRollEmbed = rollEmbed;
            tgtRollEmbed.Fields[0].Value += $": **{rollOther}**";

            await msg.ModifyAsync(null, tgtRollEmbed.Build());
            await Task.Delay(500 + rng.Next(150));

            DiscordEmbedBuilder finalEmbed = tgtRollEmbed.AddField("result", $"**`{ctx.Member.DisplayName}`** ***{result}***");
            await msg.ModifyAsync(null, finalEmbed.Build());
        }

        // [Command("str"), Description("roll strength")]
        // public async Task StrengthAsync(CommandContext ctx, [RemainingText] string action) {
        //     Player player = await PlayerHelper.GetDefaultPlayerAsync(DbConnection, ctx.Member);
        // }
    }
}
