using System;
using System.Diagnostics;
using System.Threading.Tasks;

using DSharpPlus;
using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;
using DSharpPlus.Interactivity;
using DSharpPlus.Entities;

using Guide.Entities.SQL;
using Guide.Helpers;

using SQLite;
using System.Linq;

namespace Guide.Modules {
    [Group("character"), Aliases("char", "c")]
    public class Char : BaseCommandModule {
        private SQLiteAsyncConnection DbConnection { get; }

        public Char(SQLiteAsyncConnection conn) {
            this.DbConnection = conn;
        }

        [GroupCommand]
        public async Task ExecuteAsync(CommandContext ctx, [Description("The index of the character to view")] int index = -1) {
            if(index != -1 && (index < 0 || index > 2)) {
                await ctx.RespondAsync("Invalid character. Must be between 0-2");
                return;
            }

            (Character, int) charWithIndex;
            if(index == -1)
                charWithIndex = await ctx.Member.GetCurrentCharacterWithIndexAsync(this.DbConnection);
            else charWithIndex = (await ctx.Member.GetCharacterAsync(this.DbConnection, index), index);
            Character character = charWithIndex.Item1;

            if(character == null) {
                if(index == -1)
                    await ctx.RespondAsync($"You don't have any characters. Do `{ctx.Prefix}c new` to create one.");
                else await ctx.RespondAsync("You don't have a character on this index. Try the one prior to this.");
                return;
            }

            Server serverCfg = await ctx.Guild.GetCfgAsync(this.DbConnection);

            DiscordEmbedBuilder embed = new DiscordEmbedBuilder()
                .WithTitle($"**[{character.Name}`@`{charWithIndex.Item2}]**")
                .WithDescription($"`age`: {character.Age}\n"
                    + $"`gender`: {character.Gender.ToString()}\n")
                .WithColor(DiscordColor.IndianRed);

            if(!string.IsNullOrEmpty(character.RefUrl))
                embed.WithImageUrl(character.RefUrl);

            if (serverCfg.StatsEnabled) {
                embed.AddField("Stats", $"`life`: {character.Life}/{character.LifeMax}\n"
                    + $"`strength`: {character.Defense}\n"
                    + $"`speed`: {character.Speed}\n"
                    + $"`stealth`: {character.Stealth}\n"
                    + $"`magic`: {character.Magic}");

            if(character.Bio != null)
                embed.AddField("Bio", character.Bio);
            }

            await ctx.RespondAsync(embed: embed);
        }
    
        [Command("new"), Description("create a new character")]
        public async Task NewAsync(CommandContext ctx) {
            AsyncTableQuery<Character> charTable = await ctx.Member.GetCharactersAsync(this.DbConnection);

            int count = await charTable.CountAsync();
            if(count == 3) {
                await ctx.RespondAsync("Sorry friend, you reached the max character limit (3).\n"
                + $"You have to remove a character to make another. You can request all data of a character you currently have to back it up by `{ctx.Prefix}c backup`");
                return;
            }

            // Create a new channel to ask in.
            DiscordOverwriteBuilder[] overwrites = new DiscordOverwriteBuilder[] {
                new DiscordOverwriteBuilder()
                    .Allow(Permissions.AccessChannels)
                    .For(ctx.Member),
                new DiscordOverwriteBuilder()
                    .Deny(Permissions.AccessChannels)
                    .For(ctx.Guild.EveryoneRole)
            };

            DiscordChannel channel = await ctx.Guild.CreateChannelAsync($"cnew-{ctx.Member.Id}", ChannelType.Text, overwrites: overwrites);
            await channel.RespondAsync(ctx.Member.Mention);

            InteractivityExtension interactivity = ctx.Client.GetInteractivity();
            
            // Useful methods
            async Task DeleteChannelAsync(string reason = "Cancelled") {
                await channel.RespondAsync($"{reason}. This channel will now self-destruct in 15 seconds.");
                await Task.Delay(1000 * 15);
                await channel.DeleteAsync();
            }

            async Task<string> WaitForAnswer(string prompt) {
                await channel.RespondAsync(prompt);
                var res = await interactivity.WaitForMessageAsync(m => m.ChannelId == channel.Id && m.Author.Id == ctx.Member.Id, TimeSpan.FromHours(1));
                if(res.TimedOut) {
                    await DeleteChannelAsync("Timed out");
                    throw new OperationCanceledException();
                }
                return res.Result.Content;
            }

            Character newChar = new Character(ctx.Member.Id, ctx.Guild.Id);

            // Start asking
            try {
                await channel.SendMessageAsync($"I'll now ask for the data about your character. Use `{ctx.Prefix}cancel` to cancel anytime.");

                string ans = await WaitForAnswer("What is your character's name?");
                if(ans == "$cancel") await DeleteChannelAsync();
                newChar.Name = ans;

                ans = await WaitForAnswer("What is your character's age?");
                if (ans == "$cancel") await DeleteChannelAsync();
                newChar.Age = ans;

                ans = await WaitForAnswer("What is their gender?");
                if (ans == "$cancel") await DeleteChannelAsync();
                if(!Enum.TryParse(ans.ToLowerInvariant(), true, out CharacterGender gender)) {
                    gender = CharacterGender.Other;
                }
                newChar.Gender = gender;
                if (ans == "$cancel") await DeleteChannelAsync();
                string their = gender == CharacterGender.Male ? "his" : (gender == CharacterGender.Female) ? "her" : "their";

                ans = await WaitForAnswer($"What is {their} appearance? (links only please as of right now)");
                if (ans == "$cancel") await DeleteChannelAsync();
                newChar.RefUrl = ans;

                ans = await WaitForAnswer($"Finally, what is {their} bio?");
                if (ans == "$cancel") await DeleteChannelAsync();
                newChar.Bio = ans;
            } catch(OperationCanceledException) {
                return;
            }

            Server scfg = await ctx.Guild.GetOrCreateCfgAsync(this.DbConnection);
            newChar.Life = newChar.LifeMax = scfg.StartingLifeMax;

            // Insert new character
            await this.DbConnection.InsertAsync(newChar);

            // If the user doesnt have any characters then set this one as their current/default
            if(count == 0) {
                Player playa = await ctx.Member.GetOrCreatePlayerAsync(this.DbConnection);
                playa.CurrentCharacterIndex = 1;
            }

            // Goodbye channel
            await DeleteChannelAsync("Done");
        }
    
        [Command("del"), Aliases("rm"), Description("remove a character")]
        public async Task DeleteAsync(CommandContext ctx, [Description("The index of the character to delete")] int index =  -1) {
            if (index != -1 && (index < 0 || index > 2)) {
                await ctx.RespondAsync("Invalid character. Must be between 0-2");
                return;
            }

            (Character, int) charWithIndex = await ctx.Member.GetCurrentCharacterWithIndexAsync(this.DbConnection);
            Character character = charWithIndex.Item1;

            if (character == null) {
                await ctx.RespondAsync($"You don't have any characters, Friend.");
                return;
            }

            InteractivityExtension interactivity = ctx.Client.GetInteractivity();

            DiscordEmbedBuilder embed = new DiscordEmbedBuilder()
                .WithTitle($"**Delete [{character.Name}`@`{charWithIndex.Item2}]**")
                .WithDescription("Type the name of this character to confirm.")
                .WithColor(DiscordColor.Red);

            DiscordMessage msg = await ctx.RespondAsync(embed: embed);
            var res = await interactivity.WaitForMessageAsync(m => m.Channel == ctx.Channel && m.Author == ctx.Member, TimeSpan.FromSeconds(30));
            if(res.TimedOut) {
                await msg.ModifyAsync(null, embed.WithDescription(embed.Description + " **(Timed out)**").Build());
                return;
            }

            if(res.Result.Content != character.Name) {
                await msg.ModifyAsync(null, embed.WithDescription(embed.Description + " **(Cancelled)**").Build());
                return;
            }

            await this.DbConnection.DeleteAsync<Character>(character.OwnerId);

            await ctx.RespondAsync("Done.");
        }

        [Command("ch"), Description("change your current character")]
        public async Task DefaultAsync(CommandContext ctx, [Description("The index of the character to change to")] int index) {
            if (index < 0 || index > 2) {
                await ctx.RespondAsync("Invalid character. Must be between 0-2");
                return;
            }

            Character character = await ctx.Member.GetCharacterAsync(this.DbConnection, index);

            if (character == null) {
                await ctx.RespondAsync("You don't have a character on this index. Try the one prior to this.");
                return;
            }

            Player playa = await ctx.Member.GetOrCreatePlayerAsync(this.DbConnection);
            playa.CurrentCharacterIndex = index;

            await this.DbConnection.UpdateAsync(playa);
            await ctx.RespondAsync("Done");
        }
    
        [Command("edit"), Aliases("mod"), Description("edit your current character")]
        public async Task EditAsync(CommandContext ctx) {
            (Character, int) charWithIndex = await ctx.Member.GetCurrentCharacterWithIndexAsync(this.DbConnection);
            Character character = charWithIndex.Item1;

            if (character == null) {
                await ctx.RespondAsync($"You don't have any characters, Friend.");
                return;
            }

            DiscordEmbedBuilder embed = new DiscordEmbedBuilder()
                .WithTitle($"**Editing [{character.Name}`@`{charWithIndex.Item2}**")
                .WithDescription($"`name`  **|**  `{character.Name}` \n"
                    + $"`age`         **|**  `{character.Age}` \n"
                    + $"`gender`      **|**  `{character.Gender}` \n"
                    + $"`appearance`  **|**  `{(!string.IsNullOrEmpty(character.RefUrl) ? "..." : "")}` \n"
                    + $"`bio`         **|**  `{(!string.IsNullOrEmpty(character.Bio) ? "..." : "")}`")
                .WithColor(DiscordColor.Black)
                .WithFooter($"{ctx.Prefix}cancel to cancel.");

            DiscordMessage msg = await ctx.RespondAsync(embed: embed);

            InteractivityExtension interactivity = ctx.Client.GetInteractivity();

            var res = await interactivity.WaitForMessageAsync(m => m.Channel == ctx.Channel && m.Author == ctx.Member, TimeSpan.FromMinutes(30));
            if(res.TimedOut) {
                await msg.ModifyAsync("**Timed out**");
                return;
            }

            string content = res.Result.Content.ToLowerInvariant();
            if(content == $"{ctx.Prefix}cancel") {
                await ctx.RespondAsync("Cancelled.");
                return;
            }

            async Task<string> WaitForAnswer(string prompt, TimeSpan timeout) {
                DiscordMessage message = await ctx.RespondAsync(prompt);
                var ans = await interactivity.WaitForMessageAsync(m => m.Channel == ctx.Channel && m.Author == ctx.Member, timeout);
                if(ans.TimedOut) {
                    await message.ModifyAsync(message.Content + " **(Timed out)**");
                    return null;
                }
                return ans.Result.Content;
            }

            string key = content;
            string value;
            switch(content) {
                case "name":
                    string newName = await WaitForAnswer("Enter a new name:", TimeSpan.FromSeconds(30));
                    if(newName == null) return;
                    value = character.Name = newName;
                    break;
                case "age":
                    string newAge = await WaitForAnswer("Enter a new age:", TimeSpan.FromSeconds(30));
                    if (newAge == null) return;
                    value = character.Age = newAge;
                    break;
                case "gender":
                    string newGender = await WaitForAnswer("Enter a new gender:", TimeSpan.FromSeconds(30));
                    if (newGender == null) return;
                    if(!Enum.TryParse(newGender, true, out CharacterGender gender)) {
                        gender = CharacterGender.Other;
                    }
                    character.Gender = gender;
                    value = gender.ToString();
                    break;
                case "bio":
                    string newBio = await WaitForAnswer("Enter a new bio:", TimeSpan.FromMinutes(10));
                    if (newBio == null) return;
                    character.Bio = newBio;
                    value = "...";
                    break;
                case "appearance":
                    string newRef = await WaitForAnswer("Enter a new image url:", TimeSpan.FromMinutes(5));
                    if (newRef == null) return;
                    character.RefUrl = newRef;
                    value = "...";
                    break;
                default:
                    await ctx.RespondAsync($"I don't recognize {content}. Check if you typed that right.");
                    return;
            }

            await this.DbConnection.UpdateAsync(character);

            DiscordEmoji emojiCheckmark = DiscordEmoji.FromName(ctx.Client, ":white_check_mark:");
            await ctx.RespondAsync($"{emojiCheckmark} {key} **->** {value}");
        }
    }
}
