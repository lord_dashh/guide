using System;
using System.Diagnostics;
using System.Threading.Tasks;

using DSharpPlus;
using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;
using DSharpPlus.Entities;

using Guide.Helpers;
using Guide.Entities.SQL;

using SQLite;

namespace Guide.Modules {
    [Group("servercfg"), Aliases("scfg"), RequireUserPermissions(Permissions.Administrator)]
    public class ServerCfg : BaseCommandModule {
        private SQLiteAsyncConnection DbConnection { get; }

        public ServerCfg(SQLiteAsyncConnection conn) {
            this.DbConnection = conn;
        }

        [GroupCommand]
        public async Task ExecuteAsync(CommandContext ctx) {
            Server cfg = await ctx.Guild.GetOrCreateCfgAsync(this.DbConnection);

            await ctx.RespondAsync(embed: new DiscordEmbedBuilder()
                .WithTitle($"{ctx.Guild.Name} : Configuration")
                .WithDescription($"`Custom Prefix (prefix)`: {cfg.CustomPrefix}\n"
                    + $"`Stats enabled (stats)`: {cfg.StatsEnabled}\n"
                    + $"`Time enabled (time)`: {cfg.TimeEnabled}")
                .WithColor(DiscordColor.IndianRed));
        }

        [Command("set"), Description("change the value of a key.")]
        public async Task SetAsync(CommandContext ctx, string key, [RemainingText] string value) {
            Server cfg = await ctx.Guild.GetOrCreateCfgAsync(this.DbConnection);

            switch(key) {
                case "stats":
                    if(!bool.TryParse(value, out bool val)) {
                        await ctx.RespondAsync("Invalid value given. Must be true/false");
                        return;
                    }
                    cfg.StatsEnabled = val;
                    await ctx.RespondAsync($"Updated key {key} to value {val}.");
                    if(cfg.StatsEnabled) {
                        await ctx.RespondAsync("Since you enabled stats. Please run `servercfg setup-stats` to setup stats for this server.");
                    }
                    break;
                case "prefix":
                    cfg.CustomPrefix = value;
                    await ctx.RespondAsync($"Updated key {key} to value {value}.");
                    break;
                default:
                    await ctx.RespondAsync("Key not found.");
                    return;
            }

            await DbConnection.UpdateAsync(cfg);
        }

        [Command("setup-stats"), Description("set up player stats for this server")]
        public async Task SetupStatsAsync(CommandContext ctx) {
            Server cfg = await ctx.Guild.GetOrCreateCfgAsync(this.DbConnection);

            await ctx.RespondAsync("Beginning stats setup. This setup will allow you to determine how players will start off with their stats.");

            DiscordMessage answer = await ctx.WaitForAnswerAsync("How much life will players start with?");
            if(answer == null) {
                return;
            }

            if(!int.TryParse(answer.Content, out int lifeMax)) {
                await ctx.RespondAsync("Invalid value.");
                return;
            }

            cfg.StartingLifeMax = lifeMax;

            await this.DbConnection.UpdateAsync(cfg);

            DiscordMessage updatePlayersAns = await ctx.WaitForAnswerAsync("Done. Do you wish to apply these changes to all existing players?");
            if(updatePlayersAns == null || updatePlayersAns.Content.ToLowerInvariant() != "yes") {
                await ctx.RespondAsync("Finished setting up stats without updating existing players.");
                return;
            }

            Character[] chars = await this.DbConnection.Table<Character>().ToArrayAsync();
            for (int i = 0; i < chars.Length; i++) {
                Character ch = chars[i];
                ch.LifeMax = lifeMax;
                ch.Life = lifeMax;
            }

            await this.DbConnection.UpdateAllAsync(chars);

            await ctx.RespondAsync("Finished setting up with updating existing players.");
        }
    }
}
