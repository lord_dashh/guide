using System;
using System.Linq;
using System.Threading.Tasks;

using DSharpPlus;
using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;
using DSharpPlus.Entities;
using DSharpPlus.Interactivity;

using Guide.Helpers;
using Guide.Entities.SQL;

using SQLite;

namespace Guide.Modules {
    [Group("store"), Aliases("st")]
    [RequireGuild]
    public class ItemStore : BaseCommandModule {
        private SQLiteAsyncConnection DbConnection { get; }

        public ItemStore(SQLiteAsyncConnection conn) {
            this.DbConnection = conn;
        }

        [Command("list"), Description("see the list of items this guild has")]
        public async Task ListAsync(CommandContext ctx) {
            ItemBlob dict = await ctx.Guild.GetItemDictFromCacheAsync(this.DbConnection);

            DiscordEmbedBuilder embed = new DiscordEmbedBuilder()
                .WithTitle("Items")
                .WithDescription(string.Join("\n", dict.Items.Select(kv => {
                    DiscordEmoji emoji;
                    if(kv.Value.Type == ItemType.Melee)
                        emoji = DiscordEmoji.FromName(ctx.Client, ":crossed_swords:");
                    else if(kv.Value.Type == ItemType.Ranged)
                        emoji = DiscordEmoji.FromName(ctx.Client, ":bow_and_arrow:");
                    else emoji = DiscordEmoji.FromName(ctx.Client, ":star2:");

                    return $"`{kv.Value.Id}`: {emoji} **{kv.Value.Name}**    **`{kv.Value.Damage}`**";
                })));


            await ctx.RespondAsync(embed: embed);
        }

        [Command("new"), Description("add a new item to the list")]
        [RequireUserPermissions(Permissions.Administrator)]
        public async Task AddAsync(CommandContext ctx) {
            ItemBlob dict = await ctx.Guild.GetItemDictFromCacheAsync(this.DbConnection);
            InteractivityExtension interactivity = ctx.Client.GetInteractivity();

            async Task<string> WaitForAnswerAsync(string prompt, TimeSpan timeout) {
                await ctx.RespondAsync(prompt);
                var ans = await interactivity.WaitForMessageAsync(m => m.Channel == ctx.Channel && m.Author == ctx.Member, timeout);
                if(ans.TimedOut || ans.Result.Content == $"{ctx.Prefix}cancel") {
                    await ctx.RespondAsync("Cancelled.");
                    throw new OperationCanceledException();
                }
                return ans.Result.Content;
            }

            Item newItem = new Item();

            try {
                string ans = await WaitForAnswerAsync("What is gonna be your item's name?", TimeSpan.FromSeconds(30));
                newItem.Name = ans;
                ans = await WaitForAnswerAsync("What is the type of your item? (melee, ranged, magic)", TimeSpan.FromSeconds(30));
                if(!Enum.TryParse(ans, true, out ItemType itemType)) {
                    await ctx.RespondAsync("Invalid item type");
                    return;
                }
                newItem.Type = itemType;

                ans = await WaitForAnswerAsync("What is the description of your item?", TimeSpan.FromMinutes(5));
                newItem.Desc = ans;

                ans = await WaitForAnswerAsync("How much damage does your item do?", TimeSpan.FromSeconds(30));
                if(!int.TryParse(ans, out int dmg)) {
                    await ctx.RespondAsync("Invalid item damage. Must be an integer.");
                    return;
                }
                newItem.Damage = dmg;
            } catch(OperationCanceledException) {
                return;
            }

            newItem.Id = dict.Items.Count + 1;
            dict.Items.Add(newItem.Id, newItem);
            #pragma warning disable 4014
            ItemHelper.SyncAsync(this.DbConnection);
            #pragma warning restore 4014

            DiscordEmoji checkmark = DiscordEmoji.FromName(ctx.Client, ":white_check_mark:");
            await ctx.RespondAsync($"{checkmark} Item added.");
        }
    
        [Command("give"), Description("add an item to a player")]
        [RequirePermissions(Permissions.Administrator)]
        public async Task GiveAsync(CommandContext ctx, DiscordMember member, int id) {
            ItemBlob dict = await ctx.Guild.GetItemDictFromCacheAsync(this.DbConnection);
            
            if(!dict.Items.TryGetValue(id, out Item item)) {
                await ctx.RespondAsync("Item Id not found.");
                return;
            }

            ItemBlob inventory = await member.GetInventoryFromCacheAsync(this.DbConnection);

            if (!inventory.Items.TryGetValue(id, out Item playerItem)) {
                Item clone = (Item) item.Clone();
                clone.Count = 1;
                inventory.Items.Add(id, clone);
            } else playerItem.Count++;

            DiscordEmoji checkmark = DiscordEmoji.FromName(ctx.Client, ":white_check_mark:");
            await ctx.RespondAsync($"{checkmark} Given item **`{item.Name}`** to `{member.DisplayName}`");
        }
    }
}
