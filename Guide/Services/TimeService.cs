using System;
using System.Linq;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Threading.Tasks;
using System.Timers;

using Guide.Entities.SQL;

using SQLite;

namespace Guide.Services {
    /// <summary>
    /// Responsible for managing the time in all guilds.
    /// </summary>
    public class TimeService {
        private Timer _syncTimer;
        private ConcurrentDictionary<ulong, Time> _timeDict;

        private SQLiteAsyncConnection DbConnection { get; }

        public TimeService(SQLiteAsyncConnection conn) {
            this.DbConnection = conn;

            _syncTimer = new Timer(1000 * 10); // sync every 10 minutes
            _syncTimer.Elapsed += async (s, e) => await SyncToDb(s, e);
        }

        private async Task SyncToDb(object sender, EventArgs e) {
            await DbConnection.UpdateAllAsync(_timeDict.Values);
        }

        public async Task InitializeAsync() {
            List<Time> timeList = await DbConnection.Table<Time>().ToListAsync();
            _timeDict = new ConcurrentDictionary<ulong, Time>(timeList.ToDictionary(t => (ulong) t.ServerId));
        }
    }
}
