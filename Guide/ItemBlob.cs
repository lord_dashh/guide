using System;
using System.Collections.Generic;

namespace Guide {
    public enum ItemType {
        Melee,
        Ranged,
        Magic
    }

    public class Item : ICloneable {
        public int Id { get; set; }
        public ItemType Type { get; set; }
        public string Name { get; set; }
        public string Desc { get; set; }
        public int Damage { get; set; }

        public int Count { get; set; }

        public object Clone() {
            return this.MemberwiseClone();
        }
    }

    public class ItemBlob {
        public Dictionary<int, Item> Items { get; set; }
    }
}
