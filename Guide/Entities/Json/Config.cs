namespace Guide.Entities.Json {
    public struct Config {
        public struct DiscordConfig {
            public string Token { get; set; }
            public string InviteUrl { get; set; }
            public string[] Prefixes { get; set; }
        }
        
        public struct CoreConfig {
            public string SQLiteDbPath { get; set; }
        }

        public DiscordConfig Discord { get; set; }
        public CoreConfig Core { get; set; }
    }
}
