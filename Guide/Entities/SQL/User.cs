using System;

using SQLite;

namespace Guide.Entities.SQL {
    public class Player {
        [PrimaryKey, NotNull]
        public string Id { get; set; }
        [NotNull]
        public long UserId { get; set; }
        [NotNull]
        public long ServerId { get; set; }

        public int CurrentCharacterIndex { get; set; }

        public Player() {
        }

        public Player(long id, long serverId) {
            this.ServerId = serverId;
            this.UserId = id;
            this.Id = $"{this.ServerId}:{this.UserId}";
        }

        public Player(ulong id, ulong serverId) : this((long) id, (long) serverId) {
        }
    }
}
