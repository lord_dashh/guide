using System.Timers;

using SQLite;

namespace Guide.Entities.SQL {
    public class Time {
        [PrimaryKey, NotNull]
        public long ServerId { get; set; }

        public int Days { get; set; } = 1;
        public int Hours { get; set; }
        public int Minutes{ get; set; }
        public int Seconds { get; set; }

        [Ignore]
        public Timer Timer { get; set; }

        public Time() {
        }

        public Time(ulong guildId) {
            this.ServerId = (long) guildId;
        }
    }
}
