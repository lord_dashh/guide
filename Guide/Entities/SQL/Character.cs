using SQLite;

namespace Guide.Entities.SQL {
    public enum CharacterClass {
        None,
        Melee,
        Ranged,
        Mage,
        Assassin
    }

    public enum CharacterGender {
        Male,
        Female,
        Other
    }

    public class Character {
        [PrimaryKey, NotNull]
        public string Id { get; set; }
        [NotNull]
        public long OwnerId { get; set; }
        [NotNull]
        public long ServerId { get; set; }

        public string Name { get; set; }
        public string Age { get; set; }
        public CharacterGender Gender { get; set; }
        public string Bio { get; set; }
        public string RefUrl { get; set; }

        #region Stats
        
        public int Life { get; set; }
        public int LifeMax { get; set; }
        public static int Stamina { get; set; }

        public int Defense { get; set; } = 1;
        public int Speed { get; set; } = 1;
        public int Stealth { get; set; } = 1;
        public int Magic { get; set; }

        public static CharacterClass Class { get; set; } // players class
        public int TotalBonus { get; set; } // cumulative sum of all class and unique bonuses

        public int Acrobatics { get; set; }

        #endregion

        public Character() {
        }

        public Character(long ownerId, long serverId) {
            this.OwnerId = ownerId;
            this.ServerId = serverId;
            this.Id = $"{this.ServerId}:{this.OwnerId}";
        }

        public Character(ulong ownerId, ulong serverId) : this((long) ownerId, (long) serverId) {
        }
    }
}
