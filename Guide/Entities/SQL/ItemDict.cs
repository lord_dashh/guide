using SQLite;

namespace Guide.Entities.SQL {
    public class ItemDict {
        [PrimaryKey, NotNull]
        public string Id { get; set; }
        [NotNull]
        public long ServerId { get; set; }

        public string IdBlob { get; set; }
        public string NameBlob { get; set; }
        public string DescBlob { get; set; }
        public string MaxQuantityBlob { get; set; }
        public string TypeBlob { get; set; }
        public string DamageBlob { get; set; }

        public ItemDict() {
        }

        public ItemDict(long serverId) {
            this.ServerId = serverId;
            this.Id = $"{serverId}";
        }
    }
}
