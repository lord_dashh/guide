using SQLite;

namespace Guide.Entities.SQL {
    public class Inventory {
        [PrimaryKey, NotNull]
        public string Id { get; set; }
        [NotNull]
        public long OwnerId { get; set; }
        [NotNull]
        public long ServerId { get; set; }

        public string SlotBlob { get; set; }

        public Inventory() {
        }

        public Inventory(long userId, long serverId) {
            this.OwnerId = userId;
            this.ServerId = serverId;
            this.Id = $"{serverId}:{userId}";
        }
    }
}
