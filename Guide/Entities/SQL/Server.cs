using System;

using SQLite;

namespace Guide.Entities.SQL {
    public class Server {
        [PrimaryKey, NotNull]
        public long Id { get; set; }
        public string CustomPrefix { get; set; }
        public bool StatsEnabled { get; set; } = true;
        public bool TimeEnabled { get; set; } = true;

        #region Stats

        public int StartingLifeMax { get; set; }

        #endregion

        public Server() {
        }

        public Server(ulong id) : this((long) id) {
        }

        public Server(long id) {
            this.Id = id;
        }
    }
}
