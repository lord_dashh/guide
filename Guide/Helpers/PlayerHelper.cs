using System;
using System.Threading.Tasks;

using DSharpPlus.Entities;

using Guide.Entities.SQL;

using SQLite;

namespace Guide.Helpers {
    public static class PlayerHelper {
        public static async Task<Player> GetPlayerAsync(this DiscordMember member, SQLiteAsyncConnection dbConnection) {
            return await GetPlayerAsync((long) member.Id, (long) member.Guild.Id, dbConnection);
        }

        public static async Task<Player> GetPlayerAsync(long uid, long gid, SQLiteAsyncConnection dbConnection) {
            return await dbConnection.Table<Player>().FirstOrDefaultAsync(p => p.UserId == uid && p.ServerId == gid);
        }

        public static async Task<Player> GetOrCreatePlayerAsync(this DiscordMember member, SQLiteAsyncConnection dbConnection) {
            return await GetOrCreatePlayerAsync((long) member.Id, (long) member.Guild.Id, dbConnection);
        }  

        public static async Task<Player> GetOrCreatePlayerAsync(long uid, long gid, SQLiteAsyncConnection dbConnection) {
            Player player = await GetPlayerAsync(uid, gid, dbConnection);
            if(player == null) {
                player = new Player(uid, gid);
                await dbConnection.InsertAsync(player);
            }
            return player;
        }


        public static async Task<AsyncTableQuery<Character>> GetCharactersAsync(this DiscordMember member, SQLiteAsyncConnection dbConnection) {
            return await GetCharactersAsync((long) member.Id, (long) member.Guild.Id, dbConnection);
        }

        public static async Task<AsyncTableQuery<Character>> GetCharactersAsync(long uid, long gid, SQLiteAsyncConnection dbConnection) {
            Player player = await GetOrCreatePlayerAsync(uid, gid, dbConnection);

            return dbConnection.Table<Character>().Where(c => c.OwnerId == uid && c.ServerId == gid);
        }

        public static async Task<Character> GetCharacterAsync(this DiscordMember member, SQLiteAsyncConnection dbConnection, int index) {
            return await GetCharacterAsync((long) member.Id, (long) member.Guild.Id, dbConnection, index);
        }

        public static async Task<Character> GetCharacterAsync(long uid, long gid, SQLiteAsyncConnection dbConnection, int index) {
            AsyncTableQuery<Character> charTable = await GetCharactersAsync(uid, gid, dbConnection);
            Character[] chars = await charTable.ToArrayAsync();
            if (chars == null) {
                return null;
            }

            if(index > chars.Length) {
                return null;
            }

            return chars[index];
        }

        public static async Task<Character> GetCurrentCharacterAsync(this DiscordMember member, SQLiteAsyncConnection dbConnection) {
            return await GetCurrentCharacterAsync((long) member.Id, (long) member.Guild.Id, dbConnection);
        }

        public static async Task<Character> GetCurrentCharacterAsync(long uid, long gid, SQLiteAsyncConnection dbConnection) {
            Player player = await GetOrCreatePlayerAsync(uid, gid, dbConnection);

            Character[] chars = await dbConnection.Table<Character>().Where(c => c.OwnerId == uid && c.ServerId == gid).ToArrayAsync();

            if(chars.Length == 0) {
                return null;
            }

            if (player.CurrentCharacterIndex > chars.Length) {
                return null;
            }

            return chars[player.CurrentCharacterIndex];
        }

        public static async Task<(Character, int)> GetCurrentCharacterWithIndexAsync(this DiscordMember member, SQLiteAsyncConnection dbConnection) {
            return await GetCurrentCharacterWithIndexAsync((long) member.Id, (long) member.Guild.Id, dbConnection);
        }

        public static async Task<(Character, int)> GetCurrentCharacterWithIndexAsync(long uid, long gid, SQLiteAsyncConnection dbConnection) {
            Player player = await GetOrCreatePlayerAsync(uid, gid, dbConnection);

            Character[] chars = await dbConnection.Table<Character>().Where(c => c.OwnerId == uid && c.ServerId == gid).ToArrayAsync();

            if(chars.Length == 0) {
                return (null, player.CurrentCharacterIndex);
            }

            return (chars[player.CurrentCharacterIndex], player.CurrentCharacterIndex);
        }
    }
}
