using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;

using DSharpPlus.Entities;

using Guide.Entities.SQL;

using SQLite;

namespace Guide.Helpers {
    public static class ItemHelper {
        public static Dictionary<long, ItemBlob> ItemDictCache { get; }
        public static Dictionary<string, ItemBlob> InventoryCache { get; }

        static ItemHelper() {
            ItemDictCache = new Dictionary<long, ItemBlob>();
            InventoryCache = new Dictionary<string, ItemBlob>();
        }

        public static async Task SyncAsync(SQLiteAsyncConnection dbConnection) {
            List<ItemDict> newDicts = new List<ItemDict>();

            foreach(var kv in ItemDictCache) {
                ItemDict dict = await GetOrCreateItemDictAsync(kv.Key, dbConnection);
                ItemDictParser.Serialize(ref dict, kv.Value);
                newDicts.Add(dict);
            }

            await dbConnection.UpdateAllAsync(newDicts);

            List<Inventory> newInventories = new List<Inventory>();

            foreach(var kv in InventoryCache) {
                Inventory inventory = await GetOrCreateInventoryAsync(kv.Key, dbConnection);
                InventoryParser.Serialize(ref inventory, kv.Value);
                newInventories.Add(inventory);
            }

            await dbConnection.UpdateAllAsync(newInventories);
        }

        public static async Task<ItemBlob> GetItemDictFromCacheAsync(this DiscordGuild guild, SQLiteAsyncConnection dbConnection) {
            if(ItemDictCache.TryGetValue((long) guild.Id, out ItemBlob blob)) {
                return blob;
            }

            blob = ItemDictParser.Deserialize(await GetOrCreateItemDictAsync(guild, dbConnection));
            ItemDictCache.Add((long) guild.Id, blob);

            return blob;
        }

        public static async Task<ItemDict> GetOrCreateItemDictAsync(this DiscordGuild guild, SQLiteAsyncConnection conn) {
            long gid = (long) guild.Id;
            ItemDict d = await conn.Table<ItemDict>().FirstOrDefaultAsync(i => i.ServerId == gid);
            
            if(d == null) {
                d = new ItemDict(gid);
                await conn.InsertAsync(d);
            }

            return d;
        }

        public static async Task<ItemDict> GetOrCreateItemDictAsync(long gid, SQLiteAsyncConnection conn) {
            ItemDict d = await conn.Table<ItemDict>().FirstOrDefaultAsync(i => i.ServerId == gid);

            if (d == null) {
                d = new ItemDict(gid);
                await conn.InsertAsync(d);
            }

            return d;
        }

        public static async Task<ItemBlob> GetInventoryFromCacheAsync(this DiscordMember member, SQLiteAsyncConnection dbConnection) {
            long gid = (long) member.Guild.Id;
            long uid = (long) member.Id;
            if (InventoryCache.TryGetValue($"{gid}:{uid}", out ItemBlob blob)) {
                return blob;
            }

            blob = InventoryParser.Deserialize(await GetOrCreateInventoryAsync(member, dbConnection), await GetItemDictFromCacheAsync(member.Guild, dbConnection));
            InventoryCache.Add($"{gid}:{uid}", blob);

            return blob;
        }

        public static async Task<Inventory> GetOrCreateInventoryAsync(this DiscordMember member, SQLiteAsyncConnection dbConnection) {
            long gid = (long) member.Guild.Id;
            long uid = (long) member.Id;
            Inventory inventory = await dbConnection.Table<Inventory>().FirstOrDefaultAsync(i => i.OwnerId == uid && i.ServerId == gid);

            if(inventory == null) {
                inventory = new Inventory(uid, gid);
                await dbConnection.InsertAsync(inventory);
            }

            return inventory;
        }

        public static async Task<Inventory> GetOrCreateInventoryAsync(long gid, long uid, SQLiteAsyncConnection dbConnection) {
            Inventory inventory = await dbConnection.Table<Inventory>().FirstOrDefaultAsync(i => i.OwnerId == uid && i.ServerId == gid);

            if (inventory == null) {
                inventory = new Inventory(uid, gid);
                await dbConnection.InsertAsync(inventory);
            }

            return inventory;
        }

        public static async Task<Inventory> GetOrCreateInventoryAsync(string id, SQLiteAsyncConnection dbConnection) {
            string[] ids = id.Split(':');
            long gid= long.Parse(ids[0]);
            long uid = long.Parse(ids[1]);

            Inventory inventory = await dbConnection.Table<Inventory>().FirstOrDefaultAsync(i => i.OwnerId == uid && i.ServerId == gid);

            if (inventory == null) {
                inventory = new Inventory(uid, gid);
                await dbConnection.InsertAsync(inventory);
            }

            return inventory;
        }
    }
}
