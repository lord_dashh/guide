using System.Threading.Tasks;

using DSharpPlus.Entities;

using Guide.Entities.SQL;

using SQLite;

namespace Guide.Helpers {
    public static class ServerHelper {
        public static async Task<Server> GetOrCreateCfgAsync(this DiscordGuild server, SQLiteAsyncConnection dbConnection) {
            return await GetOrCreateCfgAsync((long) server.Id, dbConnection);
        }

        public static async Task<Server> GetOrCreateCfgAsync(ulong gid, SQLiteAsyncConnection dbConnection) {
            return await GetOrCreateCfgAsync((long) gid, dbConnection);
        }

        public static async Task<Server> GetOrCreateCfgAsync(long gid, SQLiteAsyncConnection dbConnection) {
            Server serv = await GetCfgAsync(gid, dbConnection);
            
            if(serv == null) {
                serv = new Server(gid);
                await dbConnection.InsertAsync(serv);
            }

            return serv;
        }

        public static async Task<Server> GetCfgAsync(this DiscordGuild server, SQLiteAsyncConnection dbConnection) {
            return await GetCfgAsync((long) server.Id, dbConnection);
        }

        public static async Task<Server> GetCfgAsync(ulong serverId, SQLiteAsyncConnection dbConnection) {
            return await GetCfgAsync((long) serverId, dbConnection);
        }

        public static async Task<Server> GetCfgAsync(long serverId, SQLiteAsyncConnection dbConnection) {
            return await dbConnection.Table<Server>().FirstOrDefaultAsync(s => s.Id == serverId);
        }
    }
}
