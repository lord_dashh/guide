// Taken from my other project and modified the namespace.

using System;
using System.Drawing;

using DSharpPlus;

using Pastel;

namespace Guide {
    public static class Logger {
        private const string BOLD = @"\033[1m";
        private const string RESET = @"\033[0m";
        private static object _lock = new object();

        public static LogLevel LogSeverity { get; set; } = LogLevel.Info;
        public static string DateTimeFormat { get; set; } = "dd/MM/yy HH:mm:ss";

        private static string GetColorizedSeverityString(LogLevel severity) {
            switch (severity) {
                case LogLevel.Critical:
                    return severity.ToString().Pastel(Color.White).PastelBg(Color.Crimson);
                case LogLevel.Error:
                    return severity.ToString().Pastel(Color.Crimson);
                case LogLevel.Warning:
                    return severity.ToString().Pastel(Color.OrangeRed);
                case LogLevel.Info:
                    return severity.ToString().Pastel(Color.MediumSpringGreen);
                case LogLevel.Debug:
                    return severity.ToString().Pastel(Color.BlueViolet);
                default:
                    return severity.ToString(); // useless
            }
        }

        public static void Log(string source, string message, LogLevel severity, Exception ex = null) {
            if (severity <= LogSeverity) {
                lock (_lock) {
                    Console.Write("[".Pastel(Color.GhostWhite));
                    Console.Write(/*BOLD + */DateTime.Now.ToString(DateTimeFormat).Pastel(Color.BlanchedAlmond)/* + RESET*/);
                    Console.Write("] [".Pastel(Color.GhostWhite));
                    Console.Write(/*BOLD + */GetColorizedSeverityString(severity)/* + RESET*/);
                    Console.Write("] [".Pastel(Color.GhostWhite));
                    Console.Write(source.Pastel(Color.Bisque));
                    Console.Write("]: [".Pastel(Color.GhostWhite));
                    Console.Write(/*BOLD + */message.Pastel(Color.BlanchedAlmond)/* + RESET*/);
                    Console.Write("]\n".Pastel(Color.GhostWhite));

                    if(ex != null) {
                        Console.WriteLine(ex.ToString().Pastel(Color.Crimson));
                    }
                }
            }
        }

        public static void LogError(string source, string message, Exception exception) {
            Log(source, message, LogLevel.Error, exception);
        }
    }
}
