using System;
using System.IO;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using Timer = System.Timers.Timer;

using Microsoft.Extensions.DependencyInjection;

using DSharpPlus;
using DSharpPlus.CommandsNext;
using DSharpPlus.Entities;
using DSharpPlus.Interactivity;

using Guide.Entities.Json;
using Guide.Entities.SQL;
using Guide.Helpers;

using Newtonsoft.Json;

using SQLite;

namespace Guide.Core {
    /// <summary>
    /// The main application
    /// </summary>
    class Guide {
        private CancellationTokenSource _cancelTokenSource;
        private Timer _itemSyncTimer;

        public Config config;
        public DiscordShardedClient client;
        public SQLiteAsyncConnection dbConnection;

        /// <summary>
        /// Loads config &amp; sets up the client.
        /// </summary>
        private void Initialize() {
            if(!File.Exists("./Resources/cfg.json")) { // config path subject to change 
                Logger.Log(nameof(Guide), "config file does not exist", LogLevel.Critical);
                File.WriteAllText("./Resources/cfg.json", "{" + Environment.NewLine + "}");
                Logger.Log(nameof(Guide), $"A new config file has been created at {Path.GetFullPath("./Resources/cfg.json")}. Add data into it and then run again", LogLevel.Info);
                Environment.Exit(1);
            }

            try {
                config = JsonConvert.DeserializeObject<Config>(File.ReadAllText("./Resources/cfg.json"));
            } catch(JsonReaderException ex) {
                Logger.Log(nameof(Guide), "An error occured while deserializing config file", LogLevel.Critical, ex);
                Environment.Exit(1);
            }

            _cancelTokenSource = _cancelTokenSource ?? new CancellationTokenSource();

            client = new DiscordShardedClient(new DiscordConfiguration() {
                Token = config.Discord.Token,
                TokenType = TokenType.Bot,

                UseInternalLogHandler = false
            });
        }

        private async Task PostInitializeAsync() {
            client.Ready += (e) => {
                Logger.Log($"Shard {e.Client.ShardId}", "Ready", LogLevel.Info);
                return Task.CompletedTask;
            };

            client.GuildDownloadCompleted += (e) => {
                Logger.Log($"Shard {e.Client.ShardId}", $"Connected to {e.Guilds.Count} guilds.", LogLevel.Info);
                return Task.CompletedTask;
            };

            client.ClientErrored += (e) => {
                Logger.LogError($"Shard {e.Client.ShardId}", $"An event handler for {e.EventName} errored", e.Exception);
                return Task.CompletedTask;
            };

            // Set up SQL
            dbConnection = new SQLiteAsyncConnection("./Resources/database.sqlite");
            await dbConnection.CreateTableAsync<Server>();
            await dbConnection.CreateTableAsync<Player>();
            await dbConnection.CreateTableAsync<Character>();
            await dbConnection.CreateTableAsync<ItemDict>();
            await dbConnection.CreateTableAsync<Inventory>();
            await dbConnection.CreateTableAsync<Time>();

            _itemSyncTimer = new Timer(1000 * 10);
            _itemSyncTimer.Elapsed += async (s, e) => await ItemHelper.SyncAsync(dbConnection);
            _itemSyncTimer.Start();

            ServiceProvider deps = new ServiceCollection()
                .AddSingleton<SQLiteAsyncConnection>(dbConnection)
                .BuildServiceProvider();

            var cnextDict = await client.UseCommandsNextAsync(new CommandsNextConfiguration() {
                CaseSensitive = false,
                IgnoreExtraArguments = true,
                StringPrefixes = config.Discord.Prefixes,
                Services = deps
            });

            foreach(CommandsNextExtension cnext in cnextDict.Values) {
                #if DEBUG
                cnext.CommandErrored += (e) => {
                    Logger.LogError($"Shard {e.Context.Client.ShardId}", "A Command errored", e.Exception);
                    return Task.CompletedTask;
                };
                #endif

                cnext.RegisterCommands(Assembly.GetExecutingAssembly());
            }

            await client.UseInteractivityAsync(new InteractivityConfiguration());
        }

        public async Task RunAsync(CancellationTokenSource cancellationTokenSource = null) {
            _cancelTokenSource = cancellationTokenSource;
            
            Initialize();
            await PostInitializeAsync();

            try {
                await client.StartAsync();
                await Task.Delay(-1);
            } catch(TaskCanceledException) {
                Logger.Log(nameof(Guide), "Disconnecting all shards", LogLevel.Info);
                foreach(DiscordClient shard in client.ShardClients.Values) {
                    await shard.DisconnectAsync();
                }
                await ItemHelper.SyncAsync(this.dbConnection);
            }
        }
    }
}
