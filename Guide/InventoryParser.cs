using System;
using System.Linq;
using System.Collections.Generic;

using DSharpPlus.Entities;

using Guide.Entities.SQL;

namespace Guide {
    public static class InventoryParser {
        public static ItemBlob Deserialize(Inventory inventory, ItemDict dict) {
            if (string.IsNullOrEmpty(inventory.SlotBlob)) {
                ItemBlob emptyBlob = new ItemBlob();
                emptyBlob.Items = new Dictionary<int, Item>();
                return emptyBlob;
            }

            string[] slots = inventory.SlotBlob.Split(';');

            ItemBlob blob = new ItemBlob();
            blob.Items = new Dictionary<int, Item>();

            ItemBlob dictBlob = ItemDictParser.Deserialize(dict);
            for (int i = 0; i < slots.Length; i++) {
                string[] split = slots[i].Split(':');
                int id = int.Parse(split[0]);
                int amount = int.Parse(split[1]);

                Item item = dictBlob.Items[id];
                item.Count = amount;

                blob.Items.Add(id, item);
            }

            return blob;
        }

        public static ItemBlob Deserialize(Inventory inventory, ItemBlob itemDict) {
            if (string.IsNullOrEmpty(inventory.SlotBlob)) {
                ItemBlob emptyBlob = new ItemBlob();
                emptyBlob.Items = new Dictionary<int, Item>();
                return emptyBlob;
            }

            string[] slots = inventory.SlotBlob.Split(';');

            ItemBlob blob = new ItemBlob();
            blob.Items = new Dictionary<int, Item>();

            for (int i = 0; i < slots.Length; i++) {
                string[] split = slots[i].Split(':');
                int id = int.Parse(split[0]);
                int amount = int.Parse(split[1]);

                Item item = itemDict.Items[id];
                item.Count = amount;

                blob.Items.Add(id, item);
            }

            return blob;
        }

        public static void Serialize(ref Inventory inventory, ItemBlob blob) {
            string[] slots = new string[blob.Items.Count];

            for (int i = 0; i < slots.Length; i++) {
                Item item = blob.Items.ElementAt(i).Value;
                slots[i] = $"{item.Id}:{item.Count}";
            }
        }
    }
}
