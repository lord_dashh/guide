using System;
using System.Threading.Tasks;

using DSharpPlus.CommandsNext;
using DSharpPlus.Entities;
using DSharpPlus.Interactivity;

namespace Guide {
    static class Extensions {
        public static async Task<DiscordMessage> RespondAsync(this DiscordChannel channel, string content = null, bool tts = false, DiscordEmbed embed = null) {
            return await channel.SendMessageAsync(content, tts, embed);
        }

        public static async Task<DiscordMessage> WaitForAnswerAsync(this CommandContext ctx, string prompt) {
            return await WaitForAnswerAsync(ctx, prompt, TimeSpan.FromSeconds(30));
        }

        public static async Task<DiscordMessage> WaitForAnswerAsync(this CommandContext ctx, string prompt, TimeSpan timeout) {
            InteractivityExtension interactivity = ctx.Client.GetInteractivity();
            if (interactivity == null) throw new ArgumentNullException(nameof(interactivity));

            await ctx.RespondAsync(prompt);

            var res = await interactivity.WaitForMessageAsync(m => m.Channel == ctx.Channel && m.Author == ctx.Member, timeout);
            if (res.TimedOut) {
                await ctx.RespondAsync("Timed out.");
                return null;
            }

            return res.Result;
        }
    }
}
