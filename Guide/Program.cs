﻿using System.Threading.Tasks;

namespace Guide {
    class Program {
        static async Task Main(string[] args) {
            await new Guide.Core.Guide().RunAsync();
        }
    }
}
