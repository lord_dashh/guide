using System;
using System.Linq;
using System.Collections.Generic;

using Guide.Entities.SQL;

namespace Guide {
    public static class ItemDictParser {
        public static ItemBlob Deserialize(ItemDict dict) {
            if(string.IsNullOrEmpty(dict.NameBlob)) {
                ItemBlob emptyBlob = new ItemBlob();
                emptyBlob.Items = new Dictionary<int, Item>();
                return emptyBlob;
            }
            
            string[] names = dict.NameBlob.Split(';');
            int[] ids = Array.ConvertAll(dict.IdBlob.Split(';'), int.Parse);
            string[] descs = dict.DescBlob.Split(';');
            ItemType[] types = Array.ConvertAll(dict.TypeBlob.Split(';'), s => Enum.Parse<ItemType>(s, true));
            int[] dmgs = Array.ConvertAll(dict.DamageBlob.Split(';'), int.Parse);

            ItemBlob blob = new ItemBlob();
            blob.Items = new System.Collections.Generic.Dictionary<int, Item>();

            for (int i = 0; i < names.Length; i++) {
                Item item = new Item();

                item.Name = names[i];
                item.Id = ids[i];
                item.Desc = descs[i];
                item.Type = types[i];
                item.Damage = dmgs[i];

                blob.Items.Add(item.Id, item);
            }

            return blob;
        }

        public static void Serialize(ref ItemDict dict, ItemBlob blob) {
            int count = blob.Items.Count;
            string[] names = new string[count];
            int[] ids = new int[count];
            ItemType[] types = new ItemType[count];
            string[] descs = new string[count];
            int[] dmgs = new int[count];

            for (int i = 0; i < blob.Items.Count; i++) {
                Item item = blob.Items.ElementAt(i).Value;
                names[i] = item.Name;
                ids[i] = item.Id;
                types[i] = item.Type;
                descs[i] = item.Desc;
                dmgs[i] = item.Damage;
            }

            dict.NameBlob = string.Join(";", names);
            dict.IdBlob = string.Join(";", ids);
            dict.TypeBlob = string.Join(";", types);
            dict.DescBlob = string.Join(";", descs);
            dict.DamageBlob = string.Join(";", dmgs);
        }
    }
}
